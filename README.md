# usb-lights
Just some USB LED stuff here, nothing interesting.

## Parts
This project has two main parts:
 - [software](./sw)
     - sending commands to the firmware via UART (USB to UART bridge)
 - [firmware](./fw)
     - running on a microcontroller
     - listening to UART commands
     - modulating 8 LEDs (PWM/BAM) (BAM is default)

## Demo
[![usb-lights demo](https://img.youtube.com/vi/0PUIzLgWNqE/0.jpg)](https://www.youtube.com/watch?v=0PUIzLgWNqE)
