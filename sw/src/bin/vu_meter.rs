extern crate chfft;
extern crate sfml;
extern crate usb_lights;

mod audio;

use audio::*;
use usb_lights::*;

use sfml::audio::SoundRecorderDriver;
use sfml::system::Time;

use std::sync::mpsc::{channel, Receiver};

struct VuMeterAnimation {
    rx: Receiver<Vec<i16>>,
}

impl Animation for VuMeterAnimation {
    fn frame(&mut self) -> Option<Pixels> {
        self.rx.recv().ok().map(rms_db).map(|db| {
            let db_range = (-30.0, -5.0);
            let pwm = map(clamp(db, db_range), db_range, (0.0, 255.999)) as u8;
            Pixels(vec![Pixel::Monochrome(pwm); 8])
        })
    }
}

fn main() {
    let (tx, rx) = channel();
    let mut r = SampleRecorder::from_sender(tx);
    let mut driver = SoundRecorderDriver::new(&mut r);
    driver.set_processing_interval(Time::milliseconds(20));
    driver.start(44_100);

    let port = AnimationPort::new("/dev/ttyUSB0");
    let animation = VuMeterAnimation { rx };

    animation.into_message().run(port).unwrap();
}
