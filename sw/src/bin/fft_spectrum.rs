extern crate chfft;
extern crate sfml;
extern crate usb_lights;

mod audio;

use audio::*;
use usb_lights::*;

use sfml::audio::SoundRecorderDriver;
use sfml::system::Time;

use std::sync::mpsc::{channel, Receiver};

struct FftSpectrumAnimation {
    rx: Receiver<Vec<i16>>,
}

impl Animation for FftSpectrumAnimation {
    fn frame(&mut self) -> Option<Pixels> {
        self.rx.recv().ok().map(fft).map(|spectrum| {
            let bands = 8;
            let res: Vec<_> = spectrum
                .chunks(spectrum.len() / bands)
                .map(|s| s.iter().sum())
                .map(|x: f32| 10.0 * x.log10())
                .map(|x| clamp(x, (0.0, 255.999)))
                .map(|x| Pixel::Monochrome(x as u8))
                .collect();

            let debug: Vec<_> = spectrum.iter().map(|&x| x as u32).collect();
            println!("{:?}", &debug[0..30]);
            Pixels(res)
        })
    }
}

fn main() {
    let (tx, rx) = channel();
    let mut r = SampleRecorder::from_sender(tx);
    let mut driver = SoundRecorderDriver::new(&mut r);
    driver.set_processing_interval(Time::milliseconds(20));
    driver.start(44_100);

    let port = AnimationPort::new_from_write(::std::io::stdout());
    let animation = FftSpectrumAnimation { rx };

    animation.into_message().run(port).unwrap();
}
