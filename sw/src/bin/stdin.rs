extern crate usb_lights;

use std::io::stdin;
use usb_lights::*;

struct StdinAnimation {}

impl Animation for StdinAnimation {
    fn frame(&mut self) -> Option<Pixels> {
        let mut input = String::new();
        stdin().read_line(&mut input).unwrap();
        let pwm: u8 = input.trim().parse().unwrap();
        Some(Pixels(vec![Pixel::Monochrome(pwm); 8]))
    }
}

fn main() {
    let port = AnimationPort::new("/dev/ttyUSB0");
    let animation = StdinAnimation {};

    animation.into_message().run(port).unwrap();
}
