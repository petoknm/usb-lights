pub fn rms_db(samples: Vec<i16>) -> f64 {
    let sum_of_squares: f64 = samples.iter().map(|&x| x as f64).map(|x| x * x).sum();
    let power = (sum_of_squares / (samples.len() as f64)).sqrt();
    10.0 * (power / <i16>::max_value() as f64).log10()
}
