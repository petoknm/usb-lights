mod fft;
mod rms_db;
mod sample_recorder;

use std::cmp::{Ordering, PartialOrd};

pub fn map(x: f64, (xmin, xmax): (f64, f64), (ymin, ymax): (f64, f64)) -> f64 {
    ymin + (ymax - ymin) * (x - xmin) / (xmax - xmin)
}

pub fn min<T: PartialOrd>(x: T, y: T) -> T {
    match x.partial_cmp(&y) {
        Some(Ordering::Less) => x,
        _ => y,
    }
}

pub fn max<T: PartialOrd>(x: T, y: T) -> T {
    match x.partial_cmp(&y) {
        Some(Ordering::Greater) => x,
        _ => y,
    }
}

pub fn clamp<T: PartialOrd>(x: T, (xmin, xmax): (T, T)) -> T {
    max(min(x, xmax), xmin)
}

pub use self::fft::*;
pub use self::rms_db::*;
pub use self::sample_recorder::*;
