use chfft::RFft1D;

pub fn fft(samples: Vec<i16>) -> Vec<f32> {
    let mut fft = RFft1D::<f32>::new(samples.len());
    let samples_f32: Vec<f32> = samples.iter().map(|&x| x as f32).collect();
    let output = fft.forward(&samples_f32[..]);
    output.iter().map(|&c| c.norm()).collect()
}
