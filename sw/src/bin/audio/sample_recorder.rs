use sfml::audio::SoundRecorder;
use std::sync::mpsc::Sender;

pub struct SampleRecorder {
    tx: Sender<Vec<i16>>,
}

impl SampleRecorder {
    pub fn from_sender(tx: Sender<Vec<i16>>) -> SampleRecorder {
        SampleRecorder { tx }
    }
}

impl SoundRecorder for SampleRecorder {
    fn on_process_samples(&mut self, data: &[i16]) -> bool {
        let mut vec = vec![0; data.len()];
        vec.copy_from_slice(data);
        let _ = self.tx.send(vec);
        true
    }
}
