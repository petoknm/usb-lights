extern crate usb_lights;

use std::f64::consts::PI;
use usb_lights::*;

struct WaveAnimation {
    time: f64,
    time_freq: f64,
    space_freq: f64,
}

impl Animation for WaveAnimation {
    fn frame(&mut self) -> Option<Pixels> {
        self.time += 17.0 * 8.0 / 9600.0;

        Some(Pixels(
            (0..8)
                .map(|i| {
                    let angle = 2.0 * PI
                        * (self.time * self.time_freq + (i as f64) * self.space_freq / 8.0);
                    let pwm = (255.999 * (0.5 * (angle.sin() + 1.0)).powi(4)) as u8;
                    Pixel::Monochrome(pwm)
                })
                .collect(),
        ))
    }
}

fn main() {
    let port = AnimationPort::new("/dev/ttyUSB0");
    let animation = WaveAnimation {
        time: 0.0,
        time_freq: 1.0,
        space_freq: 1.0,
    };

    animation.into_message().run(port).unwrap();
}
