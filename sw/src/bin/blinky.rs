extern crate usb_lights;

use std::{thread, time};
use usb_lights::*;

struct BlinkyAnimation {
    pwm: u8,
}

impl Animation for BlinkyAnimation {
    fn frame(&mut self) -> Option<Pixels> {
        self.pwm = match self.pwm {
            0 => 255,
            _ => 0,
        };
        thread::sleep(time::Duration::from_millis(250));
        Some(Pixels(vec![Pixel::Monochrome(self.pwm); 8]))
    }
}

fn main() {
    let port = AnimationPort::new("/dev/ttyUSB0");
    let animation = BlinkyAnimation { pwm: 0 };

    animation.into_message().run(port).unwrap();
}
