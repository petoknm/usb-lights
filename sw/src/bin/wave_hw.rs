extern crate usb_lights;

use std::thread::sleep;
use std::time::Duration;
use usb_lights::*;

struct WaveMessageAnimation {
    frames: usize,
}

impl MessageAnimation for WaveMessageAnimation {
    fn frame(&mut self) -> Option<Message> {
        sleep(Duration::from_millis(100));
        match self.frames {
            0 => None,
            _ => {
                self.frames -= 1;
                Some(Message::Patch {
                    chan_id: self.frames as u8,
                    desc: ChannelDescription::Sine {
                        pwm: 255,
                        period: 10,
                    },
                })
            }
        }
    }
}

fn main() {
    let port = AnimationPort::new("/dev/ttyUSB0");
    let animation = WaveMessageAnimation { frames: 8 };

    animation.run(port).unwrap();
}
