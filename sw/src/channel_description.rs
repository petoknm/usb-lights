use std::default::Default;

#[derive(Copy, Clone)]
pub enum ChannelDescription {
    Constant { pwm: u8 },
    Sine { period: u8, pwm: u8 },
}

impl<'a> From<&'a ChannelDescription> for [u8; 2] {
    fn from(chan_desc: &'a ChannelDescription) -> Self {
        match chan_desc {
            &ChannelDescription::Constant { pwm } => [0, pwm],
            &ChannelDescription::Sine { period, pwm } => [0x80 | period, pwm],
        }
    }
}

impl Default for ChannelDescription {
    fn default() -> Self {
        ChannelDescription::Constant { pwm: 0 }
    }
}
