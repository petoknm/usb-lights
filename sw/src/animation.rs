use channel_description::ChannelDescription;
use message::Message;
use pixel::Pixel;
use std::io::{Result, Write};

pub struct Frame(pub Vec<ChannelDescription>);
pub struct Pixels(pub Vec<Pixel>);

impl From<Frame> for Message {
    fn from(mut frame: Frame) -> Self {
        frame.0.resize_default(8);
        Message::Full(array_ref![frame.0, 0, 8].clone())
    }
}

impl From<Pixels> for Frame {
    fn from(frame: Pixels) -> Self {
        Frame(
            frame
                .0
                .iter()
                .flat_map(|pixel| -> Vec<ChannelDescription> { pixel.into() })
                .collect(),
        )
    }
}

pub trait MessageAnimation: Sized {
    fn frame(&mut self) -> Option<Message>;

    fn run<P: Write>(mut self, mut port: P) -> Result<()> {
        Message::Reset.write(&mut port)?;
        while let Some(message) = self.frame() {
            message.write(&mut port)?;
            port.flush()?;
        }
        Ok(())
    }
}

pub struct FromFrameAnimation<A: FrameAnimation>(A);

impl<A: FrameAnimation> MessageAnimation for FromFrameAnimation<A> {
    fn frame(&mut self) -> Option<Message> {
        self.0.frame().map(|frame| frame.into())
    }
}

pub trait FrameAnimation: Sized {
    fn frame(&mut self) -> Option<Frame>;

    fn into_message(self) -> FromFrameAnimation<Self> {
        FromFrameAnimation(self)
    }
}

pub struct FromAnimation<A: Animation>(A);

impl<A: Animation> FrameAnimation for FromAnimation<A> {
    fn frame(&mut self) -> Option<Frame> {
        self.0.frame().map(|frame| frame.into())
    }
}

pub trait Animation: Sized {
    fn frame(&mut self) -> Option<Pixels>;

    fn into_frame(self) -> FromAnimation<Self> {
        FromAnimation(self)
    }

    fn into_message(self) -> FromFrameAnimation<FromAnimation<Self>> {
        FromFrameAnimation(self.into_frame())
    }
}
