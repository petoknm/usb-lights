use channel_description::ChannelDescription;

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Pixel {
    Monochrome(u8),
    Rgb(u8, u8, u8),
}

impl Pixel {
    pub fn brightness(&self) -> u8 {
        match self {
            &Pixel::Monochrome(v) => v,
            &Pixel::Rgb(r, g, b) => (((r as u16) + (g as u16) + (b as u16)) / 3) as u8,
        }
    }
}

impl<'a> From<&'a Pixel> for Vec<ChannelDescription> {
    fn from(pixel: &'a Pixel) -> Self {
        match pixel {
            &Pixel::Monochrome(v) => vec![ChannelDescription::Constant { pwm: v }],
            &Pixel::Rgb(r, g, b) => vec![
                ChannelDescription::Constant { pwm: r },
                ChannelDescription::Constant { pwm: g },
                ChannelDescription::Constant { pwm: b },
            ],
        }
    }
}
