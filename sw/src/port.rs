use serial::{self, SerialPort, SystemPort};
use std::io::{Result, Write};

pub struct AnimationPort<W: Write>(W);

impl<W: Write> AnimationPort<W> {
    pub fn new_from_write(w: W) -> AnimationPort<W> {
        AnimationPort(w)
    }
}

impl AnimationPort<SystemPort> {
    pub fn new(name: &str) -> Self {
        let mut port = serial::open(name).unwrap();

        port.reconfigure(&|settings| {
            try!(settings.set_baud_rate(serial::Baud9600));
            settings.set_char_size(serial::Bits8);
            settings.set_parity(serial::ParityOdd);
            settings.set_stop_bits(serial::Stop1);
            settings.set_flow_control(serial::FlowNone);
            Ok(())
        }).unwrap();

        AnimationPort(port)
    }
}

impl<W: Write> Write for AnimationPort<W> {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        self.0.write(buf)
    }

    fn flush(&mut self) -> Result<()> {
        self.0.flush()
    }
}
