#![feature(vec_resize_default)]
#[macro_use]
extern crate arrayref;
extern crate serial;

mod animation;
mod channel_description;
mod message;
mod pixel;
mod port;

pub use animation::*;
pub use channel_description::ChannelDescription;
pub use message::Message;
pub use pixel::Pixel;
pub use port::AnimationPort;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
