use channel_description::ChannelDescription;
use std::io::{Result, Write};

#[derive(Copy, Clone)]
pub enum OutputMode {
    PWM = 0x00,
    BAM = 0x01,
}

pub enum Message {
    Reset,
    Full([ChannelDescription; 8]),
    Patch {
        chan_id: u8,
        desc: ChannelDescription,
    },
    SetOutputMode(OutputMode),
}

impl Message {
    pub fn write<W: Write>(&self, mut w: W) -> Result<()> {
        match self {
            &Message::Reset => {
                w.write(&[0xFF; 17])?;
            }
            &Message::Full(ref descs) => {
                w.write(&[0x00])?;
                for desc in descs.iter() {
                    w.write(&<[u8; 2]>::from(desc))?;
                }
            }
            &Message::Patch { chan_id, desc } => {
                w.write(&[0x01, chan_id])?;
                w.write(&<[u8; 2]>::from(&desc))?;
            }
            &Message::SetOutputMode(mode) => {
                w.write(&[0x02, mode as u8])?;
            }
        }
        Ok(())
    }
}
