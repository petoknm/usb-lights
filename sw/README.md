# usb-lights-sw

Software for controlling usb-lights

## Important Types

 - ChannelDescription
    - description of how the LED should behave
 - Message
    - sent directly to the firmware
 - Frame
    - a group of `ChannelDescription`s
    - convertable into a message (Message::Full)
 - Pixels
    - a group of `Pixel`s
    - convertable into a frame

## Animations

There are three kinds of animations you can create. They each create different data to be sent to the firmware, depending on the abstration you need.

 - MessageAnimation
    - create messages that will be sent to the firmware directly
    - lowest level of abstration
 - FrameAnimation
    - create frames that will be converted into full messages and sent
    - medium level of abstration
 - Animation
    - create pixels that will be converted into frames, full messages and sent
    - highest level of abstration
