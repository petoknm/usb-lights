# usb-lights-fw
Firmware for STM8S103F3P6 microcontroller that drives 8 LEDs.

## Wiring
8 modulated outputs:
 - PC3
 - PC4
 - PC5
 - PC6
 - PC7
 - PD1
 - PD2
 - PD3

## Requirements
 - sdcc (tested on 3.6.9 #10151, 3.7.0 #10231)
 - stm8flash

## Building
```shell
mkdir build && cd build
cmake -DCMAKE_TOOLCHAIN_FILE=../sdcc.cmake ..
make
```

## Pre-built binary
You can find a pre-built binary in the form of an intel hex file in the
[bin](./bin) directory.
