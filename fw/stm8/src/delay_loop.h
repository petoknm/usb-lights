#ifndef DELAY_US_H
#define DELAY_US_H

#include <stdint.h>

inline void delay_loop(uint32_t count) {
    while (--count) { __asm__("nop"); }
}

#endif /* DELAY_US_H */
