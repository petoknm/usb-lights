#ifndef MODULATION_H
#define MODULATION_H

#include "types.h"

void modulation_set(output_mode);
void modulation_run();

#endif // MODULATION_H
