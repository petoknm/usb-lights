#ifndef PINS_H
#define PINS_H

#include "stm8s.h"

inline void pins_init() {
    PC_CR1 = 0b11111000;
    PC_DDR = 0b11111000;
    PD_CR1 = 0b00001110;
    PD_DDR = 0b00001110;
}

inline void pins_output(uint8_t data) {
    PC_ODR = data << 3;
    PD_ODR = data >> 4;
}

#endif // PINS_H
