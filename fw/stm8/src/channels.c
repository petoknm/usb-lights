#include "channels.h"
#include "sine.h"
#include <string.h>

extern uint32_t            time_ms;
static channel_description channels[8];
static uint32_t            timestamps[8];

void channels_init() {
    memset(channels, 0, sizeof(channels));
    memset(timestamps, 0, sizeof(timestamps));
}

uint8_t channel_brightness(uint8_t channel_id) {
    if (CHANNEL_DESCRIPTION_TYPE(channels[channel_id]) == CONSTANT) {
        return CHANNEL_DESCRIPTION_CONSTANT_BRIGHTNESS(channels[channel_id]);
    } else if (CHANNEL_DESCRIPTION_TYPE(channels[channel_id]) == SINE) {
        uint8_t period = CHANNEL_DESCRIPTION_SINE_PERIOD(channels[channel_id]);
        uint8_t brightness_max =
            CHANNEL_DESCRIPTION_SINE_MAX_BRIGHTNESS(channels[channel_id]);

        uint32_t ms    = time_ms - timestamps[channel_id];
        uint32_t angle = (256 * ms) / (period * 100);
        return (brightness_max * sine(angle & 0xFF)) >> 8;
    }
    return 0;
}

void channel_patch(uint8_t channel_id, channel_description chan_desc) {
    if (channels[channel_id] != chan_desc) { timestamps[channel_id] = time_ms; }
    channels[channel_id] = chan_desc;
}

void channel_full(channel_description descs[8]) {
    for (int i = 0; i < 8; i++) { channel_patch(i, descs[i]); }
}
