#include "bam.h"
#include "channels.h"
#include "delay_loop.h"
#include "pins.h"

static void bam_bit(uint8_t bit_mask) {
    uint8_t pins = 0;

    for (int i = 0; i < 8; i++) { // for every channel
        if (channel_brightness(i) & bit_mask) { pins |= 1 << i; }
    }

    pins_output(pins);
    delay_loop(bit_mask << 2);
    pins_output(0);
}

void bam_run() {
    for (uint8_t mask = 1; mask != 0; mask <<= 1) bam_bit(mask);
}
