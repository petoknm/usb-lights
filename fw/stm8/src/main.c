#include <delay.h>
#include <stdint.h>
#include <stm8s.h>

#include "channels.h"
#include "custom_stm8s.h"
#include "modulation.h"
#include "pins.h"
#include "state_machine.h"

uint32_t time_ms = 0;

static void uart1_init() {
    // Enable UART1 RX
    UART1_CR2 |= (1 << UART1_CR2_REN) | (1 << UART1_CR2_RIEN);
    // 1 stop bit
    UART1_CR3 &= ~(3 << UART1_CR3_STOP);
    // 1 parity bit, odd parity
    UART1_CR1 |=
        (1 << UART1_CR1_PCEN) | (1 << UART1_CR1_M) | (1 << UART1_CR1_PS);
    // 9600 baud (9598)
    UART1_BRR1 = 0x68;
    UART1_BRR2 = 0x03;
}

static void tim4_init() {
    /* Prescaler = 2^6 = 64 */
    TIM4_PSCR = 6;

    /* Frequency = F_CLK / (2 * prescaler * (1 + ARR))
     *           = 16 MHz / (2 * 64 * (1 + 124)) = 1 kHz */
    TIM4_ARR = 124;

    TIM4_IER |= (1 << TIM4_IER_UIE); // Enable Update Interrupt
    TIM4_CR1 |= (1 << TIM4_CR1_CEN); // Enable TIM4
}

void main() {
    // Clock divider = 0 => 16MHz
    CLK_CKDIVR = 0x00;
    // Enable UART1 and TIM4
    CLK_PCKENR1 = (1 << CLK_PCKENR1_UART1) | (1 << CLK_PCKENR1_TIM4);

    uart1_init();
    tim4_init();
    enable_interrupts();

    pins_init();
    channels_init();

    while (1) { modulation_run(); }
}

void uart1_isr() __interrupt(UART1_RXC_ISR) {
    if (UART1_SR & (1 << UART1_SR_RXNE)) { state_machine_process(UART1_DR); }
}

void tim4_isr() __interrupt(TIM4_ISR) {
    TIM4_SR &= ~(1 << TIM4_SR_UIF);
    time_ms++;
}
