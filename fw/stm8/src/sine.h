#ifndef SINE_H
#define SINE_H

#include <stdint.h>

uint8_t sine(uint8_t);

#endif // SINE_H
