#ifndef CUSTOM_STM8S_H
#define CUSTOM_STM8S_H

#define UART1_CR2_RIEN 5
#define UART1_CR3_STOP 4
#define UART1_CR1_PCEN 2
#define UART1_CR1_M 4
#define UART1_CR1_PS 1

// STM8S103F2 STM8S103F3 STM8S103K3 specific
#define CLK_PCKENR1_UART1 3
#define CLK_PCKENR1_TIM4 4

#endif // CUSTOM_STM8S_H
