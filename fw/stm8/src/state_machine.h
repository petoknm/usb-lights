#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include "types.h"
#include <stdint.h>

void state_machine_process(uint8_t);

#endif // STATE_MACHINE_H
