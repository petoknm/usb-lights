#ifndef CHANNELS_H
#define CHANNELS_H

#include "types.h"

void    channels_init();
uint8_t channel_brightness(uint8_t channel_id);
void    channel_patch(uint8_t channel_id, channel_description chan_desc);
void    channel_full(channel_description descs[8]);

#endif // CHANNELS_H
