#include "pwm.h"
#include "channels.h"
#include "pins.h"

static uint8_t count = 0;

void pwm_run() {
    uint8_t pins = 0;
    count++;
    for (int i = 0; i < 8; i++) { // for every channel
        if (count < channel_brightness(i)) { pins |= 1 << i; }
    }
    pins_output(pins);
}
