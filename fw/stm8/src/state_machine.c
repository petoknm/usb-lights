#include "state_machine.h"
#include "channels.h"
#include "modulation.h"
#include <string.h>

static uart_message_type type;
static uint8_t           rx_pos = 0;
static uint8_t           rx_buf[17];

static void reset() {
    rx_pos = 0;
    memset(rx_buf, 0, sizeof(rx_buf));
}

void state_machine_process(uint8_t byte) {
    if (rx_pos == 0) {
        type = byte;
        if (type > SET_OUTPUT_MODE) { // Invalid uart_message_type
            reset();
            return;
        }
    }

    rx_buf[rx_pos++] = byte;

    switch (type) {
    case FULL:
        if (rx_pos == 17) {
            channel_full((channel_description*)&rx_buf[1]);
            reset();
        }
        break;
    case PATCH:
        if (rx_pos == 4) {
            uint8_t channel_id = rx_buf[1] & 0x07;
            channel_patch(channel_id, rx_buf[2] | rx_buf[3] << 8);
            reset();
        }
        break;
    case SET_OUTPUT_MODE:
        if (rx_pos == 2) {
            modulation_set(rx_buf[1] & 1);
            reset();
        }
        break;
    }
}
