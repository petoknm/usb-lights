#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>

typedef enum { CONSTANT = 0, SINE = 1 } channel_description_type;

typedef enum { PWM = 0x00, BAM = 0x01 } output_mode;

typedef uint16_t channel_description;

#define CHANNEL_DESCRIPTION_TYPE(chan_desc) (chan_desc >> 15)
#define CHANNEL_DESCRIPTION_CONSTANT_BRIGHTNESS(chan_desc) (chan_desc & 0xFF)
#define CHANNEL_DESCRIPTION_SINE_PERIOD(chan_desc) ((chan_desc >> 8) & 0x7F)
#define CHANNEL_DESCRIPTION_SINE_MAX_BRIGHTNESS(chan_desc) (chan_desc & 0xFF)

typedef enum {
    FULL            = 0x00,
    PATCH           = 0x01,
    SET_OUTPUT_MODE = 0x02
} uart_message_type;

#endif // TYPES_H
