#include "modulation.h"
#include "bam.h"
#include "pwm.h"

static output_mode mode = BAM;

void modulation_set(output_mode m) { mode = m; }

void modulation_run() {
    if (mode == PWM)
        pwm_run();
    else if (mode == BAM)
        bam_run();
}
