# usb-lights-fw
Firmware for a microcontroller that drives 8 LEDs.

## Microcontrollers supported
 - [STM8S103F3P6](./stm8)

## UART Protocol

**The protocol and all diagrams are in big endian and MSB(most significant bit first) format.**

### Channel Description (2 bytes)
First bit indicates the type of channel description:

- Constant = 0

| 1 bit | 7 bits |   8 bits   |
|:-----:|:------:|:----------:|
|   0   |    X   | brightness |

- Sine = 1

| 1 bit |    7 bits    |     8 bits     |
|:-----:|:------------:|:--------------:|
|   1   | period_100ms | max_brightness |

### OutputMode (1 byte)
Different output modulation options:

 - PWM = 0x00
 - BAM = 0x01

### UART Message (2/4/17 bytes)
First byte indicates the type of message:

 - Full = 0x00

| 1 byte |        2x8 bytes       |
|:------:|:----------------------:|
|  0x00  | 8x channel_description |

 - Patch = 0x01

| 1 byte |   1 byte   |       2 bytes       |
|:------:|:----------:|:-------------------:|
|  0x01  | channel_id | channel_description |

 - SetOutputMode = 0x02

| 1 byte |    1 byte     |
|:------:|:-------------:|
|  0x02  |  output_mode  |

 - Anything else resets the state machine

### Synchronization
To prevent either of the two sides getting out of sync with the other, send 17 consequtive bytes of `0xFF`, so that the state machine gets an invalid UART message type. On the next non `0xFF` byte the state machine will be in a cleared state and ready to accept new commands.

Example:
```shell
| 17 bytes  | | UART Message ...
0xFF ... 0xFF 0x00 0x00 0x00 ...
```

## UART Communication Parameters
 - baud rate = 9600
 - data bits = 8
 - parity = odd
 - stop bits = 1
